#!/usr/bin/env python3
import sys
import argparse
import dateparser
import socket
import requests


def parse_args():
    parser = argparse.ArgumentParser(
        description="Send annotation in to timescaledb input API")
    parser.add_argument('annotation', help="Message to send")
    parser.add_argument('-t', '--timestamp', help="Timestamp for annotation",
                        default='now')
    parser.add_argument('-i', '--ignore-builtin-tags', action='store_true',
                        help="Don't put in the standard built in tags")
    parser.add_argument(
        '-u', '--url', default='http://127.0.0.1:5042/annotation/ssisystems',
        help='URL for annotation posts'
    )
    parser.add_argument(
        "--tags", metavar="KEY=VALUE", nargs='+',
        help="Set a number of key-value pairs (do not put spaces before or "
             "after the = sign). If a value contains spaces, you should "
             "define it with double quotes: 'foo=\"this is a sentence\". "
             "Note that values are always treated as strings.")
    return parser.parse_args()


def parse_var(s):
    """
    Parse a key, value pair, separated by '='
    That's the reverse of ShellArgs.

    On the command line (argparse) a declaration will typically look like:
        foo=hello
    or
        foo="hello world"
    """
    items = s.split('=')
    key = items[0].strip()  # we remove blanks around keys, as is logical
    if len(items) > 1:
        # rejoin the rest:
        value = '='.join(items[1:])
    return (key, value)


def parse_vars(items):
    """
    Parse a series of key-value pairs and return a dictionary
    """
    d = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            d[key] = value
    return d


def main():
    args = parse_args()

    # Get a real timestamp
    timestamp = dateparser.parse(args.timestamp)

    # Create some initial tags if needed
    if args.ignore_builtin_tags:
        initial_tags = {}
    else:
        initial_tags = {
            'host': socket.getfqdn()
        }

    # Combine initial tags
    arg_tags = parse_vars(args.tags)
    tags = {**initial_tags, **arg_tags}

    # create the payload
    data = {
        'annotation': args.annotation,
        'timestamp': int(timestamp.strftime("%s")),
        'tags': tags
    }
    req = requests.post(args.url, json=data, timeout=300)

    print(req.text)
    if req.json()['success']:
        print("Annotation sent")
        return 0
    else:
        sys.stderr.write("Error sending annotation\n")
        return 2

    return 0


if __name__ == "__main__":
    sys.exit(main())
