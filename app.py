#!/usr/bin/env python3

import responder
import asyncpg
import json
from datetime import datetime
import os
import sys
from prometheus_client import make_wsgi_app, Counter, Summary


if 'TDB_ADDRESS' in os.environ:
    listen = os.environ.get('TDB_ADDRESS')
else:
    listen = '127.0.0.1'

# Set up prometheus
prom_db_insert_errors = Counter(
    'tsi_db_insert_errors', 'Database Insert Errors', ['table'])
prom_processed_metrics = Counter(
    'tsi_processed_metrics', 'Processed Metrics', ['table'])
prom_db_transactions = Counter(
    'tsi_db_transactions', 'Database Transactions', ['table'])
prom_processed_annotation = Counter(
    'tsi_processed_annotation', 'Processed Annotation', ['table'])

TELEGRAF_TIME = Summary('telegraf_request_processing_seconds',
                        'Time spent processing to telegraf')
ANNOTATION_TIME = Summary('annotation_request_processing_seconds',
                          'Time spent processing annotations')

# Set up responder API
api = responder.API()


@api.on_event('startup')
async def setup_database():
    global connection_pool
    connection_pool = await asyncpg.create_pool(
        user='postgres',
        password=os.environ.get('TDB_PASSWORD'),
        database=os.environ.get('TDB_DB'),
        host=os.environ.get('TDB_HOST'),
        ssl=os.environ.get('TDB_SSL', None)
    )


@api.on_event('shutdown')
async def close_database():
    await connection_pool.close()

# Mount Prometheus_client to /metrics
api.mount('/metrics', make_wsgi_app())


@api.route("/annotation/{schema}")
class AnnotationResource:

    async def on_post(self, req, resp, *, schema):
        data = await req.media()

        statement = '''INSERT INTO {}.annotations(annotation, tags, time)
                       VALUES ($1, $2, to_timestamp($3))'''.format(schema)

        annotations = []

        # Detect if we got one or many
        if isinstance(data, list):
            for item in data:
                annotations.append(item)
        else:
            annotations = [data]

        for annotation in annotations:

            # Do we have tags?
            if 'tags' in annotation:
                tags = json.dumps(annotation['tags'])
            else:
                tags = None

            if 'timestamp' in annotation:
                timestamp = annotation['timestamp']
            else:
                timestamp = int(datetime.now().strftime("%s"))

            # Do the insert
            try:
                async with connection_pool.acquire() as connection:
                    await connection.execute(
                        statement, annotation['annotation'], tags, timestamp)
            except Exception as e:
                sys.stderr.write("Error inserting to annotations db: %s\n" % e)
                prom_db_insert_errors.labels(
                    '{}.annotation'.format(schema)).inc()

            # Count it up dawg
            prom_processed_annotation.labels(
                '{}.annotation'.format(schema)).inc()

        resp.media = {'success': True}


@api.route("/health")
class HealthResource:

    async def on_get(self, req, resp):

        statement = (
            '''select count(*) from ssisystems.telegraf '''
            '''where time >= (NOW() - INTERVAL '1 hours' )''')
        async with connection_pool.acquire() as connection:
            count = await connection.fetchval(statement)

        resp.media = {
            'success': True,
            'last_hour_count': int(count)
        }


@api.route("/telegraf/{schema}")
class TelegrafResource:

    async def on_post(self, req, resp, *, schema):

        # For multi metric stream
        data = await req.media()
        if 'metrics' in data:
            metrics = data['metrics']
        else:
            metrics = [data]

        statement = '''INSERT INTO {}.telegraf(name, fields, tags, time)
                       VALUES ($1, $2, $3, to_timestamp($4))'''.format(
                           schema)
        inserts = []
        for record_data in metrics:
            item = (
                record_data['name'],
                json.dumps(record_data['fields']),
                json.dumps(record_data['tags']),
                record_data['timestamp']
            )
            inserts.append(item)
            prom_processed_metrics.labels('{}.telegraf'.format(schema)).inc()
        try:
            async with connection_pool.acquire() as connection:
                await connection.executemany(statement, inserts)
        except Exception as e:
            sys.stderr.write("Error inserting to telegraf db: %s\n" % e)
            prom_db_insert_errors.labels(
                '{}.annotation'.format(schema)).inc()

        prom_db_transactions.labels('{}.telegraf'.format(schema)).inc()

        resp.media = {'success': True}


if __name__ == "__main__":
    api.run(address=listen)
