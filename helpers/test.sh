#!/usr/bin/env bash
set -x
set -e

http POST http://127.0.0.1:5042/telegraf/ssisystems @sample_data/multi_records.json
http POST http://127.0.0.1:5042/telegraf/ssisystems @sample_data/single_record.json

http POST http://127.0.0.1:5042/annotation/ssisystems @sample_data/single_annotation.json
http POST http://127.0.0.1:5042/annotation/ssisystems @sample_data/old_annotation.json

http http://127.0.0.1:5042/metrics | grep -v python
