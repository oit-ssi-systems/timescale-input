#!/usr/bin/env python3
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

conn = psycopg2.connect(
    "host=localhost user=postgres password=password")
cur = conn.cursor()
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur.execute("""CREATE DATABASE testing""")
cur.close()
conn.commit()

conn = psycopg2.connect(
    "host=localhost user=postgres password=password dbname=testing")
cur = conn.cursor()

cur.execute("""CREATE SCHEMA ssisystems""")

# Telegraf database stuff
cur.execute("""
CREATE TABLE ssisystems.telegraf(
  time        TIMESTAMPTZ       NOT NULL,
  name        TEXT              NOT NULL,
  fields      JSONB             NOT NULL,
  tags        JSONB
);
""")
cur.execute("""CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;""")
cur.execute("""SELECT create_hypertable('ssisystems.telegraf', 'time');""")

# Annotations table
cur.execute("""
CREATE TABLE ssisystems.annotations(
  time        TIMESTAMPTZ       NOT NULL,
  annotation  TEXT              NOT NULL,
  tags        JSONB
);
""")
cur.execute("""SELECT create_hypertable('ssisystems.annotations', 'time');""")

cur.close()
conn.commit()
